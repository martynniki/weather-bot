import telebot
from telebot import types
import requests
import sys

import configparser
config = configparser.ConfigParser()
config.read("config.ini")

API = config["bot"]["API"]
bot = telebot.TeleBot(API)

start_txt = "Доброго времени суток, это тестовый бот, предназначенный для определения погоды."

@bot.message_handler(commands=['start'])
def start(message):
    bot.send_message(message.from_user.id, start_txt, parse_mode='Markdown')
    request_txt = "Выбери желаемый доступный город"
    keyboard = types.InlineKeyboardMarkup()
    key_Moscow = types.InlineKeyboardButton(text='Москва', callback_data='Moscow')
    keyboard.add(key_Moscow)
    key_Peterburg = types.InlineKeyboardButton(text='Санкт-Петербург', callback_data='Peterburg')
    keyboard.add(key_Peterburg)
    key_Berlin = types.InlineKeyboardButton(text='Берлин', callback_data='Berlin')
    keyboard.add(key_Berlin)
    key_London = types.InlineKeyboardButton(text='Лондон', callback_data='London')
    keyboard.add(key_London)
    key_Novocheboksary = types.InlineKeyboardButton(text='Новочебоксарск', callback_data='Novocheboksary')
    keyboard.add(key_Novocheboksary)
    bot.send_message(message.from_user.id, text = request_txt, reply_markup=keyboard)

@bot.callback_query_handler(func=lambda call: True)
def callback_worker(call):
    f = open('log.txt', 'a')
    if call.data == "Moscow":
        url = "http://api.openweathermap.org/data/2.5/forecast?id=524901&appid=4095d12a0a9c96f71c94f925d6379c8e"
        city = "Москве "
        f.write('ID: ' + str(call.from_user.id))
        f.write(' City: ' +city+ '\n')
    elif call.data == "Peterburg":
        url = "http://api.openweathermap.org/data/2.5/forecast?id=498817&appid=4095d12a0a9c96f71c94f925d6379c8e"
        city = "Санкт-Петербурге "
        f.write('ID: ' + str(call.from_user.id))
        f.write(' City: ' + city + '\n')
    elif call.data == "Berlin":
        url = "http://api.openweathermap.org/data/2.5/forecast?id=2950159&appid=4095d12a0a9c96f71c94f925d6379c8e"
        city = "Берлине "
        f.write('ID: ' + str(call.from_user.id))
        f.write(' City: ' + city + '\n')
    elif call.data == "London":
        url = "http://api.openweathermap.org/data/2.5/forecast?id=2643743&appid=4095d12a0a9c96f71c94f925d6379c8e"
        city = "Лондоне "
        f.write('ID: ' + str(call.from_user.id))
        f.write(' City: ' + city + '\n')
    elif call.data == "Novocheboksary":
        url = "http://api.openweathermap.org/data/2.5/forecast?id=518976&appid=4095d12a0a9c96f71c94f925d6379c8e"
        city = "Новочебоксарске "
        f.write('ID: ' + str(call.from_user.id))
        f.write(' City: ' + city + '\n')
    f.close()
    Weather(url, city, call)



def Weather(url, city, message):
    weather_data = requests.get(url).json()
    temperature = weather_data['list'][0]['main']['temp']
    temperature = round(temperature - 273)
    temperature_feels = weather_data['list'][0]['main']['feels_like']
    temperature_feels = round(temperature_feels - 273)
    w_now = 'Сейчас в городе ' + city + ' ' + str(temperature) + ' °C'
    w_feels = 'Ощущается как ' + str(temperature_feels) + ' °C'

    precipitation = weather_data['list'][0]['weather'][0]['main']
    if precipitation == 'Snow':
        precipitation = 'На улице идет снег 🌨'
    elif precipitation == 'Rain':
        precipitation = 'На улице идет дождь 🌧️'
    elif precipitation == 'Sunny':
        precipitation = 'На улице сейчас солнечно ☀️'
    elif precipitation == 'Thunder':
        precipitation = 'На улице идет дождь с грозой, будьте осторожны ⛈️'
    elif precipitation == 'Clouds':
        precipitation = 'На улице облачно ☁️'
    wind_txt= weather_data['list'][00]['wind']['speed']
    wind_txt = 'Ветер на улице достигает '+str(wind_txt)+' метров в секунду'



    bot.send_message(message.from_user.id, w_now)
    bot.send_message(message.from_user.id, w_feels)
    bot.send_message(message.from_user.id, precipitation)
    bot.send_message(message.from_user.id, wind_txt)

if __name__ == '__main__':
    while True:
        try:
            bot.polling(none_stop=True, interval=0)
        except Exception as e:
            print('Что-то сломалось, как обычно')
            sys.exit(24)